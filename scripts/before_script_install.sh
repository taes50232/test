#!/bin/bash -xe

#
# Step 1: This file first update env with your .env file

#
# Step 2: Update necessary components and docker
# 
# 2.1 lsb_release
# 2.2 curl
# 2.3 ntp
# 2.4 docker
# 2.5 docker-compose
#
sudo apt-get -y install lsb-core
sudo apt-get -y install curl
# install ntp
sudo apt-get -y install ntp
sudo service ntp restart

# install socat
sudo apt-get install -y socat 

# install nmap
sudo apt  install nmap
    
# docker installation
sudo curl -sSL https://get.docker.com | sh
sudo usermod -aG docker root
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


# Python install 
sudo apt-get -y install python
sudo apt -y install npm
sudo apt -y install cmdtest  ## for yarn

# K8s installation 
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt-get install kubeadm kubelet kubectl -y
sudo apt-mark hold kubeadm kubelet kubectl
kubeadm version
kubelet --version

sudo hostnamectl set-hostname $HOSTNAME

sudo swapoff -a
free -m

## if you are K8s master node

# sudo kubeadm init --pod-network-cidr=10.244.0.0/16
# sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# sudo kubectl get namespaces
# sudo  kubectl get nodes
# sudo  kubectl get pods --all-namespaces
# kubectl cluster-info
# kubectl get endpoints

## See how your service  https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/


## if you want to join dlc cluster
## if token expired, you will need to check whether token valid 'kubeadm token list' and create new token by 'kubeadm token create'

# kubeadm join 10.102.4.35:6443 --token kcb8me.jbqsc4dt1n1873gf --discovery-token-ca-cert-hash sha256:3dab8c251d1328b59d5ef27f12efa0f29f55fc8f9a26f97c1663072dce884141 --v=6

#        
# Install node_exporter     
#

node_exporter_version=1.0.1

#Create User
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter
#Download Binary
sudo curl -LO https://github.com/prometheus/node_exporter/releases/download/v$node_exporter_version/node_exporter-$node_exporter_version.linux-amd64.tar.gz
#Untar 
sudo tar xvzf node_exporter-$node_exporter_version.linux-amd64.tar.gz
#Copy Expoter
sudo cp node_exporter-$node_exporter_version.linux-amd64/node_exporter /usr/local/bin/
#Assign Ownership
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
#Creating Service File
sudo cp $PWD/scripts/service/node_exporter.service /etc/systemd/system/node_exporter.service
#        
# Install Blackbox Exporter   
#

blackbox_exporter_version=0.17.0

sudo curl -LO https://github.com/prometheus/blackbox_exporter/releases/download/v$blackbox_exporter_version/blackbox_exporter-$blackbox_exporter_version.linux-amd64.tar.gz
sudo tar -xvf blackbox_exporter-$blackbox_exporter_version.linux-amd64.tar.gz

sudo cp blackbox_exporter-$blackbox_exporter_version.linux-amd64/blackbox_exporter /usr/local/bin/blackbox_exporter
sudo chown node_exporter:node_exporter /usr/local/bin/blackbox_exporter
#Creating Service File
sudo cp $PWD/scripts/service/blackbox_exporter.service /etc/systemd/system/blackbox_exporter.service

sudo mkdir /etc/blackbox_exporter
sudo cp $PWD/scripts/service/blackbox.yml /etc/blackbox_exporter/blackbox.yml

#Reload and Start
export LC_ALL=en_US.UTF-8; 
export LANG=en_US.UTF-8; 
sudo systemctl daemon-reload; 
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
sudo systemctl status node_exporter

sudo systemctl enable blackbox_exporter
sudo systemctl start blackbox_exporter
sudo systemctl status blackbox_exporter

sudo rm -rf node_exporter-$node_exporter_version.linux-amd64
sudo rm -rf blackbox_exporter-$blackbox_exporter_version.linux-amd64
sudo rm node_exporter-$node_exporter_version.linux-amd64.tar.gz
sudo rm blackbox_exporter-$blackbox_exporter_version.linux-amd64.tar.gz

exit 0
